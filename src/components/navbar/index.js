import Logo from "../../assests/images/logo.png";
import classes from "./navbar.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faCartShopping,
  faBars,
  faMultiply,
} from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";

const Navbar = () => {
  // const [scrollNav, setScrollNav] = useState(false);

  const [mobile, setMobile] = useState(false);

  // const changeBackground = () => {
  //   console.log(window.scrollY);
  //   if (window.scroll >= 40) {
  //     setScrollNav(true);
  //   } else {
  //     setScrollNav(false);
  //   }
  // };
  // window.addEventListener("scroll", changeBackground);

  return (
    <>
      <div className={classes.navbarContainer}>
        <div className={classes.logoContain}>
          <img src={Logo} alt="logo" />
        </div>
        <div className={classes.menuItems}>
          <ul
            className={`${classes.listItems} ${
              mobile && `${classes.navLinkMobile}`
            }`}
            onClick={()=>setMobile(false)}
          >
            <li>Home</li>
            <li>Layouts</li>
            <li>pages</li>
            <li>domains</li>
            <li>Hosting</li>
            <li>Blog</li>
            <li>Shop</li>
          </ul>
        </div>
        <div className={classes.navIconContainer}>
          <FontAwesomeIcon icon={faSearch} />
          <FontAwesomeIcon icon={faCartShopping} />
        </div>
        <div
          className={classes.mobileNavIcon}
          onClick={() => setMobile(!mobile)}
        >
          {mobile ? (
            <FontAwesomeIcon icon={faMultiply} size="2x" />
          ) : (
            <FontAwesomeIcon icon={faBars} size="2x" />
          )}
        </div>
      </div>
    </>
  );
};

export default Navbar;
