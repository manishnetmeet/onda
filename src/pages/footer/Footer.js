import classes from "./footer.module.scss";
import ScrollToTop from "react-scroll-to-top";
import payment1 from "../../assests/paymentLogo/payment_logo1.jpg";
import payment2 from "../../assests/paymentLogo/payment_logo2.jpg";
import payment3 from "../../assests/paymentLogo/payment_logo3.jpg";
import payment4 from "../../assests/paymentLogo/payment_logo4.jpg";
import payment5 from "../../assests/paymentLogo/payment_logo5.png";
import award1 from "../../assests/awards/award-img1.png";
import award2 from "../../assests/awards/award-img2.png";
import award3 from "../../assests/awards/award-img3.png";

const Footer = () => {
  return (
    <>
    <div className={classes.container}>
    <ScrollToTop smooth top="30" color="101010" width="20" height="20" />
    <div className={classes.subContainer}>
        <div className={classes.paymentIcons}>
          <p>We Accept:</p>
          <img src={payment1} alt="show" />
          <img src={payment2} alt="show" />
          <img src={payment3} alt="show" />
          <img src={payment4} alt="show" />
          <img src={payment5} alt="show" />
        </div>
        <div className={classes.awardsIcons}>
          <p>Awards</p>
          <img src={award1} alt="show" />
          <img src={award2} alt="show" />
          <img src={award3} alt="show" />
        </div>
        <div>
          <button className={classes.footerBtn}>live chat</button>
        </div>
      </div>
      <hr />
      <div className={classes.rightReserved}>
        <p>Copyright © 2019  | All rights reserved.</p>
      </div>
    </div>
      
    </>
  );
};

export default Footer;
