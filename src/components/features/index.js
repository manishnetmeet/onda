import ceImg from "../../assests/images/ce-3.jpg";
import classes from "./features.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
const Features = () => {
  return (
    <>
      <div className={classes.container}>
        <div>
          <img className={classes.featureImg} src={ceImg} alt="preview" />
        </div>
        <div className={classes.featuresTxt}>
          <div className={classes.title}> 
            <h4>Domain Search</h4>
          </div>
          <div className={classes.subtitle}>
            <p>
              Lorem ipsum dolor sit amet consectetuer adipiscing et justo
              Praesent mattis commodo ipsum dolor.
            </p>
          </div>
          <div className={classes.fontCircle}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
      </div>
    </>
  );
};

export default Features;
