import classes from "./offHosting.module.scss";
import Carousel from "react-bootstrap/Carousel";
import cee7 from "../../assests/images/ce-7.png";
import cee12 from "../../assests/images/ce-12.png";

const OffHosting = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.carouselContainer}>
          <Carousel className={classes.carouselSection}>
            <Carousel.Item interval={1000}>
              <div className={classes.itemsSubContainer}>
                <div className={classes.leftSection}>
                  <div className={classes.priceSticker}>
                    Start at <h3>$12</h3>
                  </div>
                  <img className={classes.sectionImg} src={cee7} alt="preview" />
                </div>
                <div className={classes.rifhtSection}>
                  <h3>Get up to </h3>
                  <h2>25% OFF</h2>
                  <h1>ON HOSTING</h1>
                  <p>
                    Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy
                    text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make{" "}
                  </p>
                  <button className={classes.readMoreButton}>READ MORE</button>
                </div>
              </div>
            </Carousel.Item>
            <Carousel.Item interval={500}>
              <div className={classes.itemsSubContainer}>
                <div className={classes.leftSection}>
                  <div className={classes.priceSticker}>
                    Start at <h3>$12</h3>
                  </div>
                  <img className={classes.sectionImg} src={cee12} alt="preview" />
                </div>
                <div className={classes.rifhtSection}>
                  <h3>Get Free </h3>
                  <h2>WEB SITE</h2>
                  <h1>BUILDER</h1>
                  <p>
                    Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy
                    text ever since th
                  </p>
                  <button className={classes.readMoreButton}>READ MORE</button>
                </div>
              </div>
            </Carousel.Item>
          </Carousel>
        </div>
      </div>
    </>
  );
};
export default OffHosting;
