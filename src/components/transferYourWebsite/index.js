import classes from "./transferYourWebsite.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faParachuteBox } from "@fortawesome/free-solid-svg-icons";
const TransferYourWebsite = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.iconSection}>
          <FontAwesomeIcon icon={faParachuteBox} size="2x" />
        </div>
        <div className={classes.txtSection}>
          <h4>Transfer Your Website</h4>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an
          </p>
        </div>
      </div>
    </>
  );
};

export default TransferYourWebsite;
