import cee8 from "../../assests/images/ce-8.jpg";
import WebsiteBuilder from "../../components/websiteBuilder";
import classes from "./affliatedProgram.module.scss";
const AffliatedProgram = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.leftSection}>
          <img className={classes.affliatedImg} src={cee8} alt="preview" />
        </div>
        <div className={classes.rightSection}>
          <div className={classes.title}>
            <h3>AFFILIATE PROGRAME</h3>
          </div>
          <h5>EARN MONEY BY AFFILIATE</h5>
          <div className={classes.contain}>
            <WebsiteBuilder />
            <WebsiteBuilder />
            <WebsiteBuilder />
          </div>
        </div>
      </div>
    </>
  );
};
export default AffliatedProgram;
