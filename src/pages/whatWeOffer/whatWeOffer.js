import Offers from "../../components/offers";
import classes from "./weOffer.module.scss";
const WhatWeOffer = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.offerTxt}>
          <h3>WHAT WE OFFER</h3>
        </div>
        <p>Get Unlimited Features</p>
      </div>
      <div className={classes.offerSection}>
        <Offers />
        <Offers />
        <Offers />
        <Offers />
      </div>
    </>
  );
};

export default WhatWeOffer;
