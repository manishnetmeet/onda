import AboutProductRecentNeed from "../../components/aboutPdctRecntNeed";
import classes from "./aboutRecent.module.scss";
import logo from "../../assests/images/d-logo.png";
import RecentPosts from "../../components/recentPosts";
const AboutProduct = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.subContainer}>
          <div>
            <div className={classes.title}>
              <h5>About Us</h5>
            </div>
            <AboutProductRecentNeed />
          </div>

          <div>
            <div>
              <h5>Products & Services</h5>
            </div>
            <AboutProductRecentNeed />
          </div>

          <div className={classes.recentPosts}>
            <div>
              <h5>Recent Posts</h5>
            </div>
            <div className={classes.posts}>
              <RecentPosts />
            </div>
            <div className={classes.posts}>
              <RecentPosts />
            </div>
            <div className={classes.posts}>
              <RecentPosts />
            </div>
          </div>

          <div>
            <div>
              <h5>Need Help?</h5>
            </div>

            <AboutProductRecentNeed />
          </div>
        </div>
        <div className={classes.content}>
          <img src={logo} alt="preview" />
          <input type="text" placeholder="Email Address" />
          <button className={classes.aboutButtom}>SUBMIT</button>
        </div>
      </div>
    </>
  );
};

export default AboutProduct;
