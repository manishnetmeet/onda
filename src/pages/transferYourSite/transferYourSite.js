import TransferYourWebsite from "../../components/transferYourWebsite";
import classes from "./transferYourSite.module.scss";
import ReactReadMoreReadLess from "react-read-more-read-less";
const TransferYourSite = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.leftSection}>
          <TransferYourWebsite />
          <ReactReadMoreReadLess
            readMoreClassName={classes.readMore}
            charLimit={200}
            readMoreText={"Read more ▼"}
            readLessText={"Read less ▲"}
          >
            sum has been the industry's standard dummy text ever since the
            1500s, when an unknown printer took a galley of type and scrambled
            it to make a type specimen book. It has survived not only five
            centuries, but also the leap into electronic typesetting, remaining
            essentially unchanged. It was popularised in the 1960s with the
            release of Letraset sheets containing Lorem Ipsum passages, and more
            recently with desktop publishing software like Aldus PageMaker
            including versions of Lorem Ipsum.
          </ReactReadMoreReadLess>
        </div>
        <div className={classes.rightSection}>
          <TransferYourWebsite />
          <ReactReadMoreReadLess
            readMoreClassName={classes.readMore}
            charLimit={200}
            readMoreText={"Read more ▼"}
            readLessText={"Read less ▲"}
          >
            sum has been the industry's standard dummy text ever since the
            1500s, when an unknown printer took a galley of type and scrambled
            it to make a type specimen book. It has survived not only five
            centuries, but also the leap into electronic typesetting, remaining
            essentially unchanged. It was popularised in the 1960s with the
            release of Letraset sheets containing Lorem Ipsum passages, and more
            recently with desktop publishing software like Aldus PageMaker
            including versions of Lorem Ipsum.
          </ReactReadMoreReadLess>
        </div>
      </div>
    </>
  );
};
export default TransferYourSite;
