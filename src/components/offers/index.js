import classes from "./offer.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLaptop, faPlus } from "@fortawesome/free-solid-svg-icons";

const Offers = () => {
  return (
    <>
      <div className={classes.container}>
        <div>
          <FontAwesomeIcon icon={faLaptop} />
        </div>
        <div className={classes.title}>
          <p>UnLimited hosting</p>
        </div>
        <div className={classes.subTitle}>
          <p >
            lorem nasnf snnfaa alksnfnfasndfn sndnnfsdnf
          </p>
        </div>
        <div className={classes.fontCircle}>
          <FontAwesomeIcon icon={faPlus} />
        </div>
      </div>
    </>
  );
};

export default Offers;
