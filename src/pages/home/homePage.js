import OndaCarousel from '../../components/carousel';
import classes from './homePage.module.scss'
const HomePage = () => {
  return (
    <>
      <div className={classes.container} >
        <OndaCarousel/>
      </div>
    </>
  );
};

export default HomePage;
