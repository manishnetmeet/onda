import cee77 from "../../assests/images/ce-7.png";
import WebsiteBuilder from "../../components/websiteBuilder";
import classes from "./createWebsite.module.scss";
const CreateWebsite = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.leftSection}>
          <div className={classes.title}>
            <h4>CREATE A WEBSITE</h4>
          </div>
          <p>CREATE YOUR OWN WEB SITE</p>
          <div className={classes.subSection}>
            <WebsiteBuilder />
            <WebsiteBuilder />
            <button className={classes.builtButton}>VIEW ALL DESIGNS</button>
            <button className={classes.builtButton}>SIGN UP NOW!</button>
          </div>
        </div>
        <div>
          <img src={cee77} alt="preview" />
        </div>
      </div>
    </>
  );
};
export default CreateWebsite;
