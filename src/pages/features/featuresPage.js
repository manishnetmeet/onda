import Features from "../../components/features";
import classes from "./features.module.scss";

const FeaturesPage = () => {
  return (
    <>
      <div>
        <div className={classes.container}>
          <div className={classes.featureSection}>
            <h4>AWESOME FEATURES</h4>
          </div>
          <p>GET UNLIMITED FEATURES</p>
        </div>
        <div className={classes.featureContains}>
          <Features />
          <Features />
          <Features />
        </div>
      </div>
    </>
  );
};

export default FeaturesPage;
