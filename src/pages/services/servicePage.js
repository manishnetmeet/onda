import Services from "../../components/servicesComps";
import classes from "./services.module.scss";
const ServicesPage = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.services}>
          <h3>OUR SERVICES</h3>
        </div>
        <p>get unlimited features</p>
      </div>
      <div className={classes.servicesContainer}>
        <Services />
        <Services /> <Services /> <Services /> <Services /> <Services />
        <Services /> <Services />
      </div>
    </>
  );
};

export default ServicesPage;
