import classes from "./recentPosts.module.scss";
import cee13 from "../../assests/images/ce-13.jpg";
const RecentPosts = () => {
  return (
    <>
      <div className={classes.container}>
        <img src={cee13} alt="preview" />
        <div className={classes.txtContent}>
          <h4>Shared Hosting</h4>
          <p>loremLorem ipsum dolor sit</p>
          <span>By John Doe June 19</span>
        </div>
      </div>
    </>
  );
};

export default RecentPosts;
