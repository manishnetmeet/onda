import Carousel from "react-bootstrap/Carousel";
import classes from "./carousel.module.scss";
import ce1 from "../../assests/ondaImagesBG/ce-1.jpg";
import ce2 from "../../assests/ondaImagesBG/ce-3.jpg";
import ce3 from "../../assests/ondaImagesBG/ce-5.jpg";
import ce6 from "../../assests/images/ce-6.png";
import cee22 from "../../assests/images/ce-2.png";
import ce4 from "../../assests/images/ce-4.png";

function OndaCarousel() {
  return (
    <div className={classes.container}>
      <Carousel>
        {/* <Carousel.Item interval={1000}> */}
        <Carousel.Item>
          <img className="d-block w-100" src={ce1} alt="First slide" />
          <Carousel.Caption>
            <div className={classes.title}>
              <h1>reliable and affordable web hosting</h1>
            </div>
            <div className={classes.priceSticker1}>Starting at $4.99/mon</div>
            <img src={ce6} alt="preview" />
          </Carousel.Caption>
        </Carousel.Item>
        {/* <Carousel.Item interval={500}> */}
        <Carousel.Item>
          <img className="d-block w-100" src={ce2} alt="Second slide" />
          <Carousel.Caption>
            <div className={classes.carouselDiv}>
              <div className={classes.firstPageText}>
                <h1>UNLIMITED WEB HOSTING</h1>
                <li>Unlimited Web Space</li>
                <li>FREE Domain Registration</li>
                <li>FREE Site-Building Tools</li>
                <li>FREE Search Engine & Marketing</li>
                <button className={classes.homePageButton}>
                  GET STARTED NOW
                </button>
              </div>
              <div>
                <div className={classes.priceOffSticker1}>Upto 50% off</div>
                <div className={classes.priceSticker2}>
                  starting at $4.99/mon
                </div>
                <img src={cee22} alt="" />
              </div>
            </div>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={ce3} alt="Third slide" />
          <Carousel.Caption>
            <div className={classes.carouselDiv2}>
              <div>
                <div className={classes.priceOffSticker}>
                  Upto <p>50%off</p>
                </div>
                <div className={classes.priceSticker}>
                  Starting at $4.99/mon
                </div>
                <img src={ce4} alt="" />
              </div>
              <div className={classes.homePageTxt}>
                <h1>DEDICATED SERVERS</h1>
                <li>Unlimited disk space</li>
                <li>Free domain registration</li>
                <li>Free site building tools</li>
                <li>Free sarch engine and Marketing </li>
                <button className={classes.homePageButton}>
                  Get Started Now
                </button>
              </div>
            </div>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

export default OndaCarousel;
