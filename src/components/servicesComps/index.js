import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEarth } from "@fortawesome/free-solid-svg-icons";
import classes from "./services.module.scss";
const Services = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.iconSection}>
          <FontAwesomeIcon
            icon={faEarth}
            size="2x"
            className={classes.iconFont}
          />
        </div>
        <div className={classes.txtContainer}>
          <h5>Dedicated Servers</h5>
          <p>
            Lorem ipsum dolor sit amet, consectetuer elit Suspendisse et dolor
            sit
          </p>
        </div>
      </div>
    </>
  );
};
export default Services;
