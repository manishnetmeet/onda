import classes from "./peopleSays.module.scss";
import Carousel from "react-bootstrap/Carousel";
import CarouselItems from "../../components/carouselItems";
const PeopleSays = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.title}>
          <h4>WHAT PEOPLE SAYS</h4>
        </div>
        <p>GET UNLIMITED FEATURES</p>
        <div className={classes.carouselContainer}>
          <Carousel className={classes.carouselSection}>
            <Carousel.Item interval={1000}>
              <div className={classes.itemsSubContainer}>
                <CarouselItems />
                <CarouselItems />
              </div>
            </Carousel.Item>
            <Carousel.Item interval={500}>
              <div className={classes.itemsSubContainer}>
                <CarouselItems />
                <CarouselItems />
              </div>
            </Carousel.Item>
            <Carousel.Item>
              <div className={classes.itemsSubContainer}>
                <CarouselItems />
                <CarouselItems />
              </div>
            </Carousel.Item>
          </Carousel>
        </div>
      </div>
    </>
  );
};
export default PeopleSays;
