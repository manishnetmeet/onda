import Pricing from "../../components/pricingComp";
import classes from "./pricing.module.scss";

const PricingPage = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.pricingSection}>
          <h4>OUR PRICING</h4>
        </div>
        <p>get unlimited features</p>
        <div className={classes.pricingContent}>
          <Pricing />
          <Pricing />
          <Pricing />
          <Pricing />
        </div>
        <div className={classes.footSction}>
        </div>
      </div>
    </>
  );
};

export default PricingPage;
