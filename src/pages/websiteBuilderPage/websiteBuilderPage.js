import WebsiteBuilder from "../../components/websiteBuilder";
import cee101 from "../../assests/images/ce-101.png";
import classes from "./website.module.scss";
const WebsiteBuilderPage = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.subContainer}>
          <div className={classes.webBuild}>
            <WebsiteBuilder />
            <WebsiteBuilder />
          </div>
          <div>
            <img className={classes.webImg} src={cee101} alt="preview" />
          </div>
          <div className={classes.webBuild}>
            <WebsiteBuilder />
            <WebsiteBuilder />
          </div>
        </div>
      </div>
    </>
  );
};
export default WebsiteBuilderPage;
