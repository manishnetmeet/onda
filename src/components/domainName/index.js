import classes from "./domainName.module.scss";
const DomainName = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.leftContainer}>
          <div className={classes.box}>
            <h5>.com</h5>
            <p>$11.99</p>
          </div>
          <div className={classes.box}>
            <h5>.org</h5>
            <p>$9.99</p>
          </div>
          <div>
            <h5>.net</h5>
            <p>$7.99</p>
          </div>
        </div>
        <div className={classes.rightContainer}>
          <input type="text" placeholder="Enter your domain name here...." />
          <select>
            <option value="A">.com</option>
            <option value="B">.net</option>
            <option value="C">.org</option>
            <option value="D">.biz</option>
            <option value="E">.us</option>
          </select>
        </div>
        <div>
          <button className={classes.domainButton}>SEARCH</button>
        </div>
      </div>
    </>
  );
};

export default DomainName;
