import classes from "./websiteBuilder.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLaptop } from "@fortawesome/free-solid-svg-icons";
const WebsiteBuilder = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.fontCircle}>
          <FontAwesomeIcon icon={faLaptop} />
        </div>
        <div className={classes.txtContainer}>
          <h5>Free Website Builder</h5>
          <p>
            Lorem ipsum dolor sit amet sit et justo lor sit amet sit et just .
          </p>
        </div>
      </div>
    </>
  );
};

export default WebsiteBuilder;
