import classes from "./whoWeAre.module.scss";
import cee1 from "../../assests/images/ce-1.jpg";
import cee2 from "../../assests/images/ce-2.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
const WeAre = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.box}>
          <img className={classes.imgName1} src={cee1} alt="preview" />
        </div>
        <div className={classes.txtBox1}>
          <div className={classes.title}>
            <h4>WHO WE ARE</h4>
          </div>
          <div className={classes.subTitle}>
            <h5>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
              Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare
              hendrerit augue Cras.
            </h5>
          </div>
          <div>
            <p className={classes.paragraph}>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
              Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare
              hendrerit augue Cras tellus In pulvinar lectus a est Curabitur
              eget orci Cras laoreet ligula Suspendisse et justo.
            </p>
          </div>
          <div className={classes.fontCircle}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
        <div className={classes.txtBox2}>
          <div className={classes.title1}>
            <h4>WHY CHOOSE US</h4>
          </div>
          <div className={classes.subTitle}>
            <h5>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
              Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare
              hendrerit augue Cras.
            </h5>
          </div>
          <div>
            <p className={classes.paragraph}>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
              Suspendisse et justo. Praesent mattis commodo augue Aliquam ornare
              hendrerit augue Cras tellus In pulvinar lectus a est Curabitur
              eget orci Cras laoreet ligula Suspendisse et justo.
            </p>
          </div>
          <div className={classes.fontCircle}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
        <div className={classes.box}>
          <img className={classes.imgName2} src={cee2} alt="preview" />
        </div>
      </div>
    </>
  );
};

export default WeAre;
