const Images = [
  {
    id: 1,
    image: "assests/icons/14.png",
  },
  {
    id: 2,
    image: "assests/icons/16.png",
  },
  {
    id: 3,
    image: "assests/icons/18.png",
  },
  {
    id: 4,
    image: "assests/icons/19.png",
  },
  {
    id: 5,
    image: "assests/icons/22.png",
  },
  {
    id: 6,
    image: "assests/icons/23.png",
  },
];

export default Images;
