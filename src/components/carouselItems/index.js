import classes from "./carouselItems.module.scss";
import cee10 from "../../assests/images/ce-10.png";
const CarouselItems = () => {
  return (
    <>
      <div className={classes.container}>
        <img className={classes.peopleImg} src={cee10} alt="preview" />
        <div className={classes.containerText}>
          <p className={classes.containerPara}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
          </p>
          <p className={classes.txtSpan}>Michael Nicholas</p>
          <p className={classes.txtSpan}>
            Developer- <span>Mediatricks</span>
          </p>
        </div>
      </div>
    </>
  );
};
export default CarouselItems;
