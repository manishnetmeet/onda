import classes from "./topServices.module.scss";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import WebsiteBuilder from "../../components/websiteBuilder";
import WhyChooseUs from "../../components/whyChooseUs";

const TopServices = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.leftSide}>
          <div className={classes.title}>
            <h3>Top Services</h3>
          </div>
          <h5>GET UNLIMITED FEATURES</h5>
          <div className={classes.itemsContainer}>
            <WebsiteBuilder />
            <WebsiteBuilder />
            <WebsiteBuilder />
          </div>
        </div>
        <div className={classes.rightSide}>
          <div className={classes.rightTitle}>
            <h3>Why Choose Us</h3>
          </div>
          <h5>GET UNLIMITED FEATURES</h5>
          <div className={classes.tabsContainer}>
            <Tabs>
              <TabList>
                <Tab>HOME PAGES</Tab>
                <Tab>PSD FILES</Tab>
                <Tab>BRANDING</Tab>
              </TabList>
              <TabPanel>
                <div className={classes.tabContent}>
                  <WhyChooseUs />
                </div>
              </TabPanel>
              <TabPanel>
                <div className={classes.tabContent}>
                  <WhyChooseUs />
                </div>
              </TabPanel>
              <TabPanel>
                <div className={classes.tabContent}>
                  {" "}
                  <WhyChooseUs />
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </div>
    </>
  );
};
export default TopServices;
