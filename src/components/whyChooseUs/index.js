import classes from "./whyChooseUs.module.scss";
import cee11 from "../../assests/images/ce-11.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

const WhyChooseUs = () => {
  return (
    <>
      <div className={classes.container}>
        <div className={classes.imgContainer}>
          <img src={cee11} alt="prew" className={classes.wcuImg} />
        </div>
        <div className={classes.txtContainer}>
          <h5>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's
          </h5>
          <p>asdkljas nasdkjansjjan dsjfkjas dsa</p>
          <p>asdkljas nasdkjansjjan dsjfkjas dsa</p>
          <div className={classes.circleFont}>
            <FontAwesomeIcon icon={faPlus} className={classes.colorFont}/>
          </div>
        </div>
      </div>
    </>
  );
};

export default WhyChooseUs;
