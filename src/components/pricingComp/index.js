import classes from "./pricing.module.scss";
const Pricing = () => {
  return (
    <>
      <div className={classes.container}>
        <h3>Basic</h3>
        <div className={classes.pricingTag}>
          <h4>$9.99</h4>
          <p>/mon</p>
        </div>
        <div className={classes.txt}>
          <p>1 GB Bandwidth</p>
        </div>
        <div className={classes.txt}>
          <p>256 MB Memory</p>
        </div>
        <div className={classes.txt}>
          <p>Free Domain Name</p>
        </div>
        <button className={classes.pricingButton}>Order Now</button>
      </div>
      
    </>
  );
};

export default Pricing;
