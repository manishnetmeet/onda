import classes from "./image.module.scss";
// import Images from "../../components/images";
import ce14 from "../../assests/icons/14.png";
import cee16 from "../../assests/icons/16.png";
import cee18 from "../../assests/icons/18.png";
import cee19 from "../../assests/icons/19.png";
import cee22 from "../../assests/icons/22.png";
import cee23 from "../../assests/icons/23.png";
const ImagePage = () => {
  return (
    <>
      <div className={classes.container}>
        {/* { Images && Images.map((img)=>
      <div key={img.id}>
        <img src={img.image} className={classes.imgClass} alt='preview'/>
      </div>
      )} */}
      
          <img src={ce14} alt="prev" className={classes.imgClass} />
          <img src={cee16} alt="prev" className={classes.imgClass} />
          <img src={cee18} alt="prev" className={classes.imgClass} />
          <img src={cee19} alt="prev" className={classes.imgClass} />
          <img src={cee22} alt="prev" className={classes.imgClass} />
          <img src={cee23} alt="prev" className={classes.imgClass} />
        
      </div>
    </>
  );
};

export default ImagePage;
